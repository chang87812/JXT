//消息系统
/*

 eventkey callbackfunction attrs

 用途：帮助各个元素之间传递消息。目前主要是click事件，目的是让处于编辑状态的元素获知用户的编辑焦点已经离开了。

 1 元素被点击，进入编辑
 2 向jxt核心订阅click事件，需要参数：源元素类型，源id，要监听的事件，要监听的元素类型。后两个参数可用*表示所有。
 3 向jxt核心广播自己的click事件，需要参数：源元素类型，源id，click
 4 收到click事件 获知用户的编辑焦点已经离开，向jxt核心取消订阅click事件

 跨frame使用 onMessage 实现。hop为防止消息在frame之间往返广播设置了跳数为1 ，跨越一次变为0. 只能跨越1次。
 */

jxt.msg = {};

jxt.msg.subList = []; //订阅列表 ：[tagname][id][eventType]
//本页面广播事件
jxt.msg.radio = function (id, eventkey, attrs, hop) {

    if (hop != 0 && hop != 1) hop = 1;
    var dellist = [];
    for (var i = 0; i < jxt.msg.subList.length; i++) {
        var ary = jxt.msg.subList[i];
        if (id == ary.source_id) continue;
        if (ary.eventkey == eventkey) {
            var fun = ary.callbackfunction;
            if ($.type(fun) == 'string') {
                jxt.route.exe(fun, null, ary.source_id, ary.eventkey, attrs, id);
            } else {
                fun(ary.source_id, ary.eventkey, attrs, id);
            }

            if (ary.subtype == "once") {
                dellist.push(i);
            }
        }
    }
    for (var i = dellist.length - 1; i >= 0; i--) {
        var c = dellist[i];
        jxt.msg.subList.splice(c, 1);
    }
    // 向其他frame发消息
    if (hop == 1) {
        var w;
        if (iframewoindow) {
            w = iframewoindow;
        } else {
            if (parentwindow) {
                w = parentwindow;
            }
        }
        if (w) {
            w.postMessage({id: id, eventkey: eventkey, attrs: attrs, hop: 0}, '*');
        }
    }
};
//订阅
jxt.msg.subscribe = function (source_id, eventkey, callbackfunction, subtype) {
    if (jxt.msg.eventkeyList.indexOf(eventkey) == -1)return false;

    for (var i = 0; i < jxt.msg.subList.length; i++) {
        var obj = jxt.msg.subList[i];
        if (obj.source_id == source_id && obj.eventkey == eventkey) return;
    }
    jxt.msg.subList.push({
        source_id: source_id,
        eventkey: eventkey,
        callbackfunction: callbackfunction,
        subtype: subtype
    });
};
//取消订阅
jxt.msg.aboutSubscribe = function (source_id, eventkey) {

    for (var i = 0; i < jxt.msg.subList.length; i++) {
        var obj = jxt.msg.subList[i];
        if (obj.source_id == source_id && obj.eventkey == eventkey) {
            jxt.msg.subList.splice(i, 1);
            break;
        }
    }

};

//跨域frame事件
var onMessage = function (e) {
    if (!e.data || !e.data.id || !e.data.eventkey) return;
    jxt.msg.radio(e.data.id, e.data.eventkey, e.data.attrs, e.data.hop);

};
window.addEventListener('message', onMessage, false);
//以下两个参数，只能同时有一个被负值
var iframewoindow; //子级frame
var parentwindow; //父级


jxt.msg.eventkeyList = ['editbegin', 'editend', 'help_file', 'help_img', 'help_link',
    'creatediv', 'addmainnode', 'addchildnode', 'nodechange', 'createlist', 'createtextlist', 'createtable'
    , 'createform', 'createselfinfo', 'createjob_intention', 'createtextdiv', 'createself_evaluation', 'creatework_experience',
    'createproject_experience','createimglist'
];