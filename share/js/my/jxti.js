/* 接口。
 */
var jxt = { edit: true, data: [],eleid:null, nowediteleid: null, nowlevoneid: null };

//系统级标签
jxt.tag = {};
jxt.pages = {};
jxt.root = {};
jxt.bar = {};
//基本标签
jxt.text = {};
jxt.img = {};
jxt.file = {};
jxt.link = {};
//结构标签
jxt.div = {};
jxt.textdiv = {};
jxt.table = {};
jxt.list = {};
jxt.textlist = {};
//自定义结构标签
jxt.form = {};
//个人信息
jxt.selfinfo = {};
//求职期望
jxt.job_intention={};
//自我评价
jxt.self_evaluation={};
//工作经历
jxt.work_experience={};
//项目经验
jxt.project_experience={};
//图片列表
jxt.imglist={};