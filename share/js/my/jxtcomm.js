//节点渲染
/*function nodeRender(node, renderchild, edit, parentTag) {
 var id = node.id;
 var ele = $("#" + id);
 var tagname = jxtJSname(node, "render");
 var _html_ = jxt[tagname].render(node, edit, parentTag);
 ele.replaceWith(_html_);
 if (!renderchild) return;
 var children = jxt[tagname].children(node);
 if (children) {
 for (var i = 0; i < children.length; i++) {
 var n = nodeselect(null, children[i]);
 nodeRender(n, renderchild, edit, node.tpname);

 }
 }

 }*/
//随机数
function GetRandomNum(Min, Max) {
    var Range = Max - Min;
    var Rand = Math.random();
    return parseInt(Min + Math.round(Rand * Range));
}
//节点查找
function nodeselect(node, jxtid) {
    if (!node) node = jxt.data;
    for (var i = 0; i < node.length; i++) {
        var nitem = node[i];
        var id = nitem.id;
        if (id == jxtid) return node[i];
    }

}
//由函数名获取可执行的js包名（tagname）
function jxtJSname(node, funname) {
    var tagname = node.type;
    if (jxt[tagname] && jxt[tagname][funname]) {
        return tagname;
    }

    /*  if (node.jsname) {
     tagname = node.jsname;
     if (jxt[tagname] && jxt[tagname][funname]) {
     return tagname;
     }
     }*/
    return "tag";

}
//获得当前节点id
/*function getNowEle(ele) {

 while (ele.id != jxt.eleid) {
 if ($(ele).attr("id") == ele.id) return ele.id;
 ele = $(ele).parent();
 if (ele) ele = ele[0];
 }
 }*/
//获得当前一级节点id
function getLevOneEle(nodes, eleid) {

    if (!eleid) return null;
    if (jxt.eleid == eleid) return null;
    var _eleid = eleid;
    while (_eleid != jxt.eleid) {
        var n = nodeselect(nodes, _eleid);
        if (!n) return null;
        if (n.temp.pid == jxt.eleid) return n.id;
        _eleid = n.temp.pid
    }
}

function eleExits(id) {
    if ($("#" + id).length > 0) {
        return true;
    } else {
        return false;
    }
}

function getFunc(w, functionname) {
    if (!w || !functionname)return;
    var arr=null;
    if( $.type(functionname)=="string" )
     arr = functionname.split('.');
    else arr=functionname;
    if (!arr || $.type(arr) != "array" || arr.length == 0)return;
    var obj = w;
    for (var i = 0; i < arr.length; i++) {
        //console.log(arr[0]+" "+arr[1]+" "+arr[2]+" ");
        obj = obj[arr[i]];
    }
    if(!obj &&  arr.length>2){
        arr[1]="tag";
       return getFunc(w,arr);
    }

    return (obj);
}

String.prototype.startWith = function (str) {
    var reg = new RegExp("^" + str);
    return reg.test(this);
};

String.prototype.endWith = function (str) {
    var reg = new RegExp(str + "$");
    return reg.test(this);
};