<script src="/share/js/my/upload.js"></script>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <div id="uploader" class="wu-example">
                    <form>
                        <table style="width:100%">
                            <tr>
                                <td>
                                    <img style="width:50px;height: 50px; " id="imgshow"
                                         onclick="$('#fileinput').click();"/>
                                </td>
                                <td>
                                    <div id="thetext" class="uploader-list">

                                        <p> 文字：<input id="_text" name="_text"/></p>

                                    </div>
                                    <div id="thelist" class="uploader-list">

                                        <p> 点击后打开链接：<input id="_url" name="_url"/></p>

                                    </div>
                                    <div id="thefile">

                                        <input type="file" id="fileinput" class="file_upload">
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" onclick="upload_ok();" class="btn btn-primary">OK</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script>
    $(function ($) {
        jxt.msg.subscribe("index", "help_file", help_upload);
        jxt.msg.subscribe("index", "help_img", help_upload);
        jxt.msg.subscribe("index", "help_link", help_upload);
        $('#myModal').on('hidden.bs.modal', function (e) {
            event_sourceid = null;
            event_callback_function = null;
        });

    });
    function uploadfinish(res_text) {

        var json = JSON.parse(res_text);
        var src = json.files[0].webpath;
        //$("#imgshow").attr("src", url);
        //$("#_url").val( url);
        setShow({src:src});
    }

    function upload_ok() {
        if (event_sourceid  && event_key) {
            var node=   event_attrs.node;
            //jxt.route.getFunc(node.type+ '.help_callback',iframewoindow)(event_sourceid, event_key, getdata());
            jxt.route.exe(node.type+ '.help_callback',iframewoindow,event_sourceid, event_key, getdata());
            //var fun = getFunc(iframewoindow, event_callback_function);
            //alert( fun);
            //fun(event_sourceid, event_key, getdata());
            event_sourceid = null;
            event_callback_function = null;
            event_attrs=null;
        }
    }

    var event_sourceid = null;
    var event_callback_function = null;
    var event_key = null;
    var event_attrs=null;
    function help_upload(subscribe_id, eventkey, attrs, event_source_id) {
        event_key = eventkey;
        event_attrs=attrs;
        var node=   event_attrs.node;
        var _data={};
        switch (node.type)
        {
            case "file":
            case "link":
            case "img":
                //jxt.route.getFunc(node.type+ '.help_getdata')(id);
                _data=node;
                break;
        }
        $('form')[0].reset();
        $("#imgshow").attr("src", "");
        setShow(_data);
        $('#myModal').modal('show');
        event_sourceid = event_source_id;
        //event_callback_function = attrs.callbackfunction;


    }
    function getdata() {
        switch (event_key) {
            case "help_file":
                return {text: $("#_text").val(), url: $("#_url").val()};
                break;
            case "help_img":
                return {src: $("#imgshow").attr("src"), url: $("#_url").val()};

                break;
            case "help_link":
                return {text: $("#_text").val(), url: $("#_url").val()};
                break;

        }
    }
    function setShow(data) {
        switch (event_key) {
            case "help_file":
                if (data) {
                    $("#_url").val((data.url?data.url:data.src));
                    $("#_text").val(data.text);
                }
                $("#imgshow").hide();
                $("#thetext").show();
                $("#thefile").show();
                break;
            case "help_img":
                if (data) {
                    $("#imgshow").attr("src", data.src);
                    var url=data.url;
                    if(!url) url=data.src;
                    $("#_url").val(url);
                }
                $("#imgshow").show();
                $("#thetext").hide();
                $("#thefile").hide();
                break;
            case "help_link":
                if (data) {
                    $("#_url").val((data.url?data.url:data.src));
                    $("#_text").val(data.text);
                }
                $("#imgshow").hide();
                $("#thetext").show();
                $("#thefile").hide();
                break;

        }
    }
</script>
