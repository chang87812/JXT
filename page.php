<!DOCTYPE html>
<html lang="zh-cn">

<head>
    <meta charset="utf-8">
  
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>htmledit</title>

    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap.min.css">

    <!-- 可选的Bootstrap主题文件（一般不用引入） -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.0/css/bootstrap-theme.min.css">

    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>

    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="http://cdn.bootcss.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="/share/js/he/template.js"></script>
    <!-- <script src="/share/js/he/stickUp.min.js"></script>-->
    <script src="/share/js/my/jxti.js"></script>
    <script src="/share/js/my/jxtcomm.js"></script>
    <script src="/share/js/my/jxtMsg.js"></script>
    <script src="/share/js/my/jxtRoute.js"></script>
    <script src="/share/js/my/jxtdata.js"></script>
    <script src="/share/js/my/jxt.js"></script>

    <?php
    $GLOBALS["tags"] = ["tag","root",  "div", "img","file","link", "list", "table", "text",'textlist','form','textdiv','work_experience','project_experience','imglist'];
    $GLOBALS["templates"] = ["root",  "div", "img","file","link", "olist", "ulist", "table", "text",'otextlist','utextlist','form','cform','formlist','textdiv','work_experience','project_experience','imglist'];
    include_once($_SERVER['DOCUMENT_ROOT'] . '/tags/include.php');
    include_once($_SERVER['DOCUMENT_ROOT'] . '/templates/include.php');
    
    $page = array_key_exists("p", $_GET) ? $_GET['p'] : "1";
    ?>
    <script>
        var pageid = '<?php  echo $page; ?>';
    </script>
</head>

<body>
<br>

<div id="textview">
    <div id="_jxtroot_">
    </div>
</div>


<script>
    if (window.parent) parentwindow = window.parent;
    localStorageid = jxt.root.dataJson.id + pageid;
    $("#_jxtroot_").attr("id", localStorageid);
    var HEdata = [];

    var _data = localStorage.getItem(localStorageid);

    if (_data) {
        HEdata = JSON.parse(_data);
    }


    jQuery(function ($) {
       

            // $("#textview").click(jxt.click);

            if (HEdata.length >= 0) {
                jxt.data = HEdata;
            }

            jxt.init("root", {
                pageid: pageid
            });
       

    });
</script>

</body>

</html>